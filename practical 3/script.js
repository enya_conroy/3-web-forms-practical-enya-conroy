console.log("hello");

function greetName()
{ 
    var inputName = document.forms["nameForm"]["userName"].value;
     alert("Hi there, " + inputName);
}

function complimentName()
{
    var inputSurname = document.forms["surnameForm"]["surname"].value;
    alert(inputSurname + "? Thats a nice name.")
}

function validatePlace()
{
    var inputLocation = document.forms["placeForm"]["location"].value;
    alert("I had a friend from " + inputLocation + " once.")
}

function favouriteColour()
{
    var inputColour = document.forms["colourForm"]["colour"].value;
    alert(inputColour + " is my favourite colour too!")
}

function foodAllergy()
{
    var inputFood = document.forms["foodForm"]["food"].value;
    alert("I'm allergic to " + inputFood + " :(")
}
